const problem1 = require('../problem1');
const folder = './JSONfolder';


problem1.createJSONDirectory(folder)
    .then((result) => {
        console.log(result);
        const filesArray = filesMakingArray(5);
        creatingJsonFiles(folder, filesArray);
    })
    .catch((error) => {
        console.error(error);
    })

function creatingJsonFiles(folder, filesArray) {
    problem1.createAllFiles(folder, filesArray)
        .then((result) => {
            console.log(result);
            deletingJsonFiles(folder, filesArray);

        })
        .catch((error) => {
            console.error(error);
        })

}
function deletingJsonFiles(folder, filesArray) {
    problem1.deleteAllFiles(folder, filesArray)
        .then((result) => {
            console.log(result);
        })
        .catch((error) => {
            console.error(error);
        })
}

function filesMakingArray(numberOfFiles) {
    const arr = []
    for (let index = 0; index < numberOfFiles; index++) {
        arr.push(`file${index}.json`);
    }
    return arr;
}

