const problem2 = require('../problem2');
const path = require('path');

const fileData = "lipsum.txt";

problem2.readingFunction(fileData)
    .then((response) => {
        console.log("File read successfully");
        return response;
    })
    .then((response) => {
        return problem2.convertUpperCase(response);
    })
    .then((response) => {
        return problem2.convertLowerCase(response);
    })
    .then((response) => {
        return problem2.sort(response);
    })
    .then(() => {
        return problem2.deleteFile();
    })
    .then((response) => {
        console.log(...response);
    })
    .catch((error) => {
        console.error(error);
    })