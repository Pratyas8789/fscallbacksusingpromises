const fs = require('fs');
const { resolve } = require('path');
const path = require('path');

function createJSONDirectory(folder) {
    return new Promise((resolve, reject) => {
        fs.access(folder, error => {
            if (error) {
                console.log("Directory not found, creating directory ....");
                fs.mkdir(folder, error => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve("directory created successfully");
                    }
                })
            } else {
                resolve("Directory already created");
            }
        })
    })
}

function createAllFiles(folder, arrayOfFiles) {

    return new Promise((resolve, reject) => {
        const err = [];
        const created = [];
        let execuatedCount = 0;
        arrayOfFiles.forEach(fileName => {
            fs.writeFile(path.join(folder, fileName), 'w', error => {
                if (error) {
                    err.push(error);
                } else {
                    created.push(fileName);
                }
                execuatedCount++;

                if (execuatedCount === arrayOfFiles.length) {

                    if (created.length === arrayOfFiles.length) {
                        resolve("All files created successfully");
                    } else {
                        reject("All files are not successfully created");
                    }
                }
            })


        });
    })
}

function deleteAllFiles(folder, arrayOfFiles) {
    return new Promise((resolve, reject) => {
        const err = [];
        const deleted = [];
        let execuatedCount = 0;
        arrayOfFiles.forEach(fileName => {
            fs.unlink(path.join(folder, fileName), error => {
                if (error) {
                    err.push(error);
                } else {
                    deleted.push(fileName);
                }
                execuatedCount++;

                if (execuatedCount === arrayOfFiles.length) {

                    if (deleted.length === arrayOfFiles.length) {
                        resolve("All files deleted successfully");
                    } else {
                        reject("All files are not successfully deleted");
                    }
                }
            })


        });
    })
}


module.exports.createJSONDirectory = createJSONDirectory;
module.exports.createAllFiles = createAllFiles;
module.exports.deleteAllFiles = deleteAllFiles;